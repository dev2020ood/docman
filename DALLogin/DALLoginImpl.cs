﻿using DalInfraContracts.Interfaces;
using DocManContracts.Interfaces;
using System;
using System.Data;

namespace DALLogin
{
    public class DALLoginImpl : IDALLogin
    {
        IInfraDAL _dal;
        public DALLoginImpl(IInfraDAL dal)
        {
            _dal = dal; 
        }
        public DataSet GetUser(string userID)
        {
            
            IDBParameter p_userID = _dal.GetParameter("UserID", userID);
            IDBConnection connection = _dal.GetConnection(""); 
            return  _dal.Exec(connection, "GetUser", p_userID);
            

        }
    }
}
