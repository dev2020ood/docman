﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DocManContracts.Interfaces
{
    public interface IDALLogin
    {
        public DataSet GetUser(string userID); 
    }
}
