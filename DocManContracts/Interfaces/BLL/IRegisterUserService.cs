﻿using DocManContracts.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocManContracts.Interfaces.BLL
{
    public interface IRegisterUserService
    {
        RegisterUserResponse RegisterUser(RegisterUserRequest request); 
    }
}
