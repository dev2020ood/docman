﻿using DocManContracts.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocManContracts.Interfaces.BLL
{
    public interface IUserLoginService
    {
        LoginResponse Login(LoginRequest request);
    }
}
