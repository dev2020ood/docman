﻿using DalInfraContracts.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocManContracts.DTO
{
    public class UserDTO
    {
        [DBParameter("UserID")]
        public string UserID { get; set; }
        [DBParameter("UserName")]
        public string UserName { get; set; }
    }
}
