﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocManContracts.DTO
{
    public class LoginResponse:Response
    {

    }
    public class LoginResponseOK
    {
        public string UserTokenID { get; set; }
    }
    public class LoginResponseInvalidUser : LoginResponse
    {

    }

}
