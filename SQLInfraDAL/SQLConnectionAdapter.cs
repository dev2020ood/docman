﻿using DalInfraContracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace SQLInfraDAL
{
    public class SQLConnectionAdapter:IDBConnection
    {
        public SqlConnection Connection { get; }
        public SQLConnectionAdapter(string connectionString)
        {
            Connection = new SqlConnection(connectionString);
        }

    }
}
