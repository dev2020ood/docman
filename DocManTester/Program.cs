﻿using DalInfraContracts.DTO;
using DalInfraContracts.Interfaces;
using DalParametersConverter;
using SQLInfraDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;

namespace DocManTester
{
    class UserDTO
    {
        [DBParameter("UserID")]
        public string UserID { get; set; }
        [DBParameter("UserName")]
        public string UserName { get; set; }

    }
    class Program
    {
        static void GetUsersWithSQLStatement()
        {
            //String connection to database 

            var conString = "Server = LENOVO-PC; Database = DocMarkersAppDB; Trusted_Connection = True;";
            //Define Connection object
            SqlConnection con = new SqlConnection(conString);

            //Define Command 
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from users";
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            DataSet retval = new DataSet();

            dataAdapter.Fill(retval);

            foreach(DataRow row in retval.Tables[0].Rows)
            {
                Console.WriteLine("Result is {0},{1}", row["UserID"],row["UserName"]);
            }


        }
        static void GetUser(string userID) //Call to sp GetUser with parameter userI
        {
            //String connection to database 
            
            var conString = "Server = LENOVO-PC; Database = DocMarkersAppDB; Trusted_Connection = True;";
            //Define Connection object
            SqlConnection con = new SqlConnection(conString);

            //Define Command 
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "GetUser"; //Which stored procedure to call 
            cmd.CommandType = CommandType.StoredProcedure;

            //Paramters 
            SqlParameter p_userID = new SqlParameter();
            p_userID.ParameterName = "userID";
            p_userID.Value = userID;
            //Connect Parameters to command
            cmd.Parameters.Add(p_userID);


            //Execute stored procedure 

            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            DataSet retval = new DataSet();

            dataAdapter.Fill(retval);

            Console.WriteLine("Result is {0},{1}", retval.Tables[0].Rows[0]["UserID"],
               retval.Tables[0].Rows[0]["UserName"]);





        }
        static void CreateUser(string userID,string userName)
        {
            var conString = "Server = LENOVO-PC; Database = DocMarkersAppDB; Trusted_Connection = True;";
            //Connection 
            //Command
            //Parameter

            SqlConnection con = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "InsertUser";
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter p_userID = new SqlParameter();
            p_userID.ParameterName = "userID";
            p_userID.Value = userID;
            SqlParameter p_userName = new SqlParameter();
            p_userName.ParameterName = "userName";
            p_userName.Value = userName;
            cmd.Parameters.Add(p_userID);
            cmd.Parameters.Add(p_userName);

            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            DataSet retval = new DataSet();

            dataAdapter.Fill(retval);

            Console.WriteLine("Result is {0},{1}", retval.Tables[0].Rows[0]["UserID"],
                retval.Tables[0].Rows[0]["Status"]);

        }
        static void Main(string[] args)
        {
            /*var id = Guid.NewGuid();
            Console.WriteLine(id);
            //GetUser("yosi357@gmail.com");
            //GetUsersWithSQLStatement();
            var conString = "Server = LENOVO-PC; Database = DocMarkersAppDB; Trusted_Connection = True;";
            IInfraDAL infraDAL = new InfraDAL();
            IDBConnection connection = infraDAL.GetConnection(conString);
            IDBParameter userid = infraDAL.GetParameter("userID", "avraham@dev.com"); 
            DataSet retval = infraDAL.Exec(connection, "GetUser", userid);

            Console.WriteLine("Result is {0},{1}", retval.Tables[0].Rows[0]["UserID"],
               retval.Tables[0].Rows[0]["UserName"]);*/
            var numberofCalls = 10000000;
            IDBParameter[] parameters1 = null;
            IDBParameter[] parameters2 = null;
            IDBParameter[] parameters3 = new IDBParameter[3];
            var dal = new InfraDAL();
            var conv = new DBParameterConverter(new InfraDAL());
            var sw = new Stopwatch();
            sw.Start(); 
            for (var i = 0; i < numberofCalls; i++)
            {
                var user = new UserDTO() { UserID="A"+i,UserName="B"+i};
                parameters1 = conv.ConvertToParameters(user);
            }
            sw.Stop();

            Console.WriteLine("With reflection:{0} {1}",sw.Elapsed,parameters1[0].Value);
            conv.RegisterTypes<UserDTO>();
            sw.Restart();
            for (var i = 0; i < numberofCalls; i++)
            {
                var user = new UserDTO() { UserID = "A" + i, UserName = "B" + i };
                parameters2 = conv.ConvertToParameters2<UserDTO>(user);
            }

            sw.Stop();
            Console.WriteLine("With Expression:{0} {1}", sw.Elapsed, parameters2[0].Value);


            sw.Restart();
            for (var i = 0; i < numberofCalls; i++)
            {
                var user = new UserDTO() { UserID = "A" + i, UserName = "B" + i };
                parameters3 = new IDBParameter[2];
                parameters3[0] = dal.GetParameter("UserID", user.UserID);
                parameters3[1] = dal.GetParameter("UserName", user.UserName);
                
            }

            sw.Stop();
            Console.WriteLine("With Native Call:{0} {1}", sw.Elapsed, parameters3[0].Value);









        }
    }
}
