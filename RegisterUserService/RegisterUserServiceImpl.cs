﻿using DalInfraContracts.Interfaces;
using DalParametersConverter;
using DocManContracts.DTO;
using DocManContracts.Interfaces.BLL;
using System;

namespace RegisterUserService
{
    public class RegisterUserServiceImpl : IRegisterUserService
    {
        IInfraDAL _dal;
        DBParameterConverter _paramConverter;
        public RegisterUserServiceImpl(IInfraDAL dal)
        {
            _dal = dal;
            _paramConverter = new DBParameterConverter(_dal);
        }
        public RegisterUserResponse RegisterUser(RegisterUserRequest request)
        {
            //GetUser, RegisterUser
            
            var conString = "Server = LENOVO-PC; Database = DocMarkersAppDB; Trusted_Connection = True;";
            IDBConnection connection = _dal.GetConnection(conString);
            //IDBParameter userid = _dal.GetParameter("UserID", request.User.UserID);
            var userid = _paramConverter.ConvertToParameter(request.User,"UserID");
            var userResult = _dal.Exec(connection, "GetUser", userid);
            //var userResult = _dal.Exec(connection, "GetUser", userid);
            RegisterUserResponse retval = new UserExistsResponse();
            if (userResult.Tables[0].Rows.Count == 0)
            {
                //IDBParameter userid2 = _dal.GetParameter("UserID", request.User.UserID);
                //IDBParameter userName = _dal.GetParameter("UserName", request.User.UserName);
                var parameters = _paramConverter.ConvertToParameters2<UserDTO>(request.User);
                
                var registerUserResult = _dal.Exec(connection, "InsertUser", parameters);
                //var registerUserResult = _dal.Exec(connection, "InsertUser", userid2, userName);
                retval = new RegisterUserResponseOK();
            }

            return retval;
        }



        
    }
}
