using DocManContracts.DTO;
using NUnit.Framework;
using RegisterUserService;
using SQLInfraDAL;

namespace RegisterUserTest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void RegisterUserOK()
        {
            var dal = new InfraDAL();
            var service = new RegisterUserServiceImpl(dal);
            var registerUserRequest = new RegisterUserRequest();
            registerUserRequest.User = new UserDTO();
            registerUserRequest.User.UserID = "1236@gmail.com";
            registerUserRequest.User.UserName = "John2";
            var retval = service.RegisterUser(registerUserRequest);

            Assert.IsInstanceOf(typeof(RegisterUserResponseOK), retval);
        }
        [Test]
        public void RegisterUserExists()
        {
            var dal = new InfraDAL();
            var service = new RegisterUserServiceImpl(dal);
            var registerUserRequest = new RegisterUserRequest();
            registerUserRequest.User = new UserDTO(); 
            registerUserRequest.User.UserID = "avraham@dev.com";
            registerUserRequest.User.UserName = "Avraham";
            var retval = service.RegisterUser(registerUserRequest);

            Assert.IsInstanceOf(typeof(UserExistsResponse), retval);
        }

        [Test]
        public void DummyTest()
        {
            var a = 3;
            var b = 5;

            Assert.AreEqual(8, a + b);
            
        }


    }
}