﻿using DalInfraContracts.Interfaces;
using System;
using System.ComponentModel;
using System.Data;
using System.Linq;

namespace MemoryDAL
{
    public class InfraDAL : IInfraDAL
    {

        public DataSet Exec(IDBConnection connection, string spName, params IDBParameter[] parameters)
        {
            var dataSet = new DataSet();
            var dataTable = new DataTable();
            dataSet.Tables.Add(dataTable);
            dataTable.Columns.Add("ID", typeof(int));

            var dataRow = dataTable.NewRow();
            dataRow["ID"] = 5;

            var dt = dataTable.AsEnumerable();
            var dt2 = dt.Where(item => item.Field<int>("ID") == 5);

            
            return dataSet; 
        }
    



        public IDBConnection GetConnection(string connectionString)
        {
            throw new NotImplementedException();
        }

        public IDBParameter GetParameter(string paramName, object paramValue)
        {
            throw new NotImplementedException();
        }
    }
}
