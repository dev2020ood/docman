﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocManContracts.DTO;
using DocManContracts.Interfaces.BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DocManApi.Controllers
{
    [Route("api/[controller]/{Action}")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        IUserLoginService _service;
        public LoginController(IUserLoginService service)
        {
            _service = service; 
        }
        [HttpPost]
        public LoginResponse Login(LoginRequest request)
        {
            try
            {
                return _service.Login(request);
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }
    }
}
