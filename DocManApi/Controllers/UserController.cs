﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocManContracts.DTO;
using DocManContracts.Interfaces.BLL;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DocManApi.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class UserController : ControllerBase
    {
        IRegisterUserService _service;
        public UserController(IRegisterUserService service)
        {
            _service = service;
        }
        
        // POST api/<UserController>
        [HttpPost]
        public RegisterUserResponse RegisterUser([FromBody] RegisterUserRequest registerUserRequest)
        {
            
            return _service.RegisterUser(registerUserRequest);
        }


        
    }
}
